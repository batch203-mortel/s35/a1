const express = require("express");
// Mongoose is a package that allows us to create Schemas to model our data structures and to manipulate our database using different access methods.
const mongoose = require("mongoose");

const app = express();
const port = 3001;

app.use(express.json()); // Allows app to read json data
app.use(express.urlencoded({extended:true})); // Allows your app to read data from forms.

// [SECTION] MongoDB Connection
	// Syntax:
		/*
			mongoose.connect("<MongoDB Atlas Connection String>",
				{
					//Allows us to avoid any current and future errors while connecting to mongoDB.
					useNewUrlParser: true,
					useUnifiedTopology: true
				}
			);
		*/
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.u3qmsla.mongodb.net/batch203_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "connection error"));

	db.once("open", () => console.log("We're connected to the cloud database."));

	// [SECTION] Mongoose Schemas
		// Schemas determine the structure of the documents to be written in the database
		// Schemas act as blueprints to our data
		// Syntax:
			/*
				const schemaName = new mongoose.Schema({<keyvalue:pair>});
			*/
		// name & status
		// "required" is used to specify that a field must not be empty.
		// "default" is used if a field value is not supplied.
		const taskSchema = new mongoose.Schema({
			name: {
				type: String,
				required: [true, "Task name is required"]
			},
			status:{
				type: String,
				default: "pending"
			}
		})

		// [SECTION] Models
			// Uses schema and use it to create/instatiate documents/object that follows our schema structure.
			// The variable/object that will be create can be used to run commandas for interacting with our database.

			/*
			Syntax: 
				const VariableName = mongoose.model("collectionName", schemaName);

				The first parameter of the Mongoose model method indicates the collection in where to store the data

				The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
			*/			
			// Task & "Task" is both capitalized following the MVC approach for naming conventions

			// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form

			const Task = mongoose.model("Task", taskSchema);

			const userSchema = new mongoose.Schema({
			userName: {
				type: String,
				required: [true, "Username is required"]
			},
			password:{
				type: String,
				required: [true, "Password is required"]
			}
		});
			
			const User = mongoose.model("User", userSchema);

// middlewares


// Business Logic
/*
    1. Add a functionality to check if there are duplicate tasks
        - If the task already exists in the database, we return an error message "Duplicate task found"
        - If the task doesn't exist in the database, we add it in the database
        A task doesn't exists if:
            - result from the query is not null
            - result.name is equal to req.body.name
    2. The task data will be coming from the request's body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

	app.post("/tasks", (request, response) =>{
		Task.findOne({name:request.body.name}, (err, result) => {
			if(result != null && request.body.name == result.name){
				return response.send("Duplicate task found!")
			}
			else{
				// "newTask" was created/instantiated from Mongoose schema and will gain access to ".save" method
				let newTask = new Task({
					name: request.body.name
				})

				newTask.save((saveErr, savedTask)=>{
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						return response.status(201).send("New Task Created.")
					}
				})
			}
		})
	});

	app.get("/tasks", (request, response) => {
		Task.find({}, (err, result) =>{
			if(err){
				return console.log(err);
			}
			else{
				return response.status(200).send({
					data: result
				})
			}
		})
	});

/*Instructions s35 Activity:
1. Using User model.
2. Create a POST route that will access the "/signup" route that will create a user.
5. Process a POST request at the "/signup" route using postman to register a user.
6. Create a git repository named S35.
7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
8. Add the link in Boodle.*/

		app.post("/signup", (request, response) => {
			User.findOne({userName: request.body.userName}, (err, result) => {
				if(result != null && request.body.userName == result.userName){
					return response.send("Username is already taken.")
				}
				else{
					let newUser = new User({
						userName: request.body.userName,
						password: request.body.password
					})
				}

					newUser.save((saveErr, saveUser)=>{
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						return response.status(201).send("New User added.")
					}
				} )
			})
		});

app.listen(port, () => console.log(`Server running at port ${port}`));